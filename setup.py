#!/usr/bin/python
# -*- coding: utf-8 -*-
from setuptools import setup
# install pip install -e .

requirements = ['numpy','scipy']


setup(name='toolsj',
      version='0.1',
      description='deploy util functions for 2afc task',
      url='',
      author='',
      author_email='',
      license='',
      packages=['toolsJ'],
      zip_safe=False,
      install_requires=requirements)
