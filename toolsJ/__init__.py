# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
pending
"""
import numpy as np
import os
from scipy.signal import firwin, lfilter # filters
#from scipy.fftpack import fft # fourier transf.
from tkinter import Tk, Frame, Button, StringVar, OptionMenu, Label, N, W, E, S


def getWaterCalib(board):
    '''returns tupple of floats(L,C,R) valvetime for a given box (boardname inside .npy calib-file is req to match). Still dont know what happens if non-existen str is used as arg
    in our setups boards are usually named as BPOD01, BPOD02, etc. This should be (or was) stored in VAR_BOX [perhaps VAR_BOARD would be clearer, but...]
    to-do: add second arguments= ul to look for in calibration file-
    '''
    watervalues = np.load(os.path.expanduser('~/pluginsr-for-pybpod/water-calibration-plugin/DATA/water_calibration_hystory.npy')).item()
    idx = next(i for i,v in zip(range(len(list(watervalues['board']))-1, -1, -1), reversed(list(watervalues['board']))) if v == board)
    return watervalues['results'][0][idx], watervalues['results'][1][idx], watervalues['results'][2][idx]

def getBroadbandCalib(xonaridx):
    '''returns tupple (L,R) of amp [latter calib values] of a given soundcard index (same as SoundR.getDevices())
    to-do: add second argument, dB or range of freq to look for.
    '''
    broadband = np.load(os.path.expanduser('~/pluginsr-for-pybpod/sound-calibration-plugin/DATA/broadbandnoise_hystory.npy'), allow_pickle=True).item()
    Lidx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='L')
    Ridx = next(i for i,v,w in zip(range(len(list(broadband['Bsoundcard']))-1, -1, -1), reversed(list(broadband['Bsoundcard'])), reversed(list(broadband['Bside']))) if v.startswith(str(xonaridx)) and w=='R')
    return (broadband['BAmp'][Lidx], broadband['BAmp'][Ridx])

def whiteNoiseGen(amp, band_fs_bot, band_fs_top, duration, FsOut=192000, Fn=10000):
	''' whiteNoiseGen(amp, band_fs_bot, band_fs_top):
	amp: float, amplitude
	band_fs_bot: int, bottom freq of the band
	band_fs_top: int, top freq
	duration: secs
	FsOut: SoundCard samplingrate to use (192k, 96k, 48k...)
	Fn: filter len, def 10k
	*** if this takes too long try shortening Fn or using a lower FsOut ***
	adding some values here. Not meant to change them usually.
	'''
	mean = 0
	std = 1

	if type(amp) is float and isinstance(band_fs_top, int) and isinstance(band_fs_bot, int) and band_fs_bot<band_fs_top:
		band_fs = [band_fs_bot, band_fs_top]
		white_noise = amp * np.random.normal(mean, std, size=FsOut * (duration + 1))
		band_pass = firwin(Fn, [band_fs[0]/(FsOut*0.5), band_fs[1]/(FsOut*0.5)], pass_zero=False) 
		band_noise = lfilter(band_pass, 1, white_noise)
		s1 = band_noise[FsOut:FsOut*(duration+1)]
		return s1 # use np.zeros(s1.size) to get equal-size empty vec.
	else:
		raise ValueError('whiteNoiseGen needs (float, int, int, num,) as arguments')

def pureToneGen(amp, freq, toneDuration, FsOut=192000):
	'''generates a given parameters pure tone vector. Gen counterpart using np.empty(s1.shape[?])
	pureToneGen(amp, freq, toneDuration, FsOut=192000):
	'''
	if type(amp) is float and type(freq) is int:
		tvec = np.linspace(0, toneDuration, toneDuration * FsOut)
		s1 = amp * np.sin(2 * np.pi * freq * tvec)
		return s1
	else:
		raise ValueError('pureToneGen needs (float, int) as arguments')


def envelope(coh, whitenoise, dur, nframes, samplingR=192000, variance=0.06, randomized=False, paired=True, LAmp=1.0, RAmp=1.0):
  '''coh: coherence from 0(left only)to 1(right). ! var < coh < (1-var). Else this wont work
  whitenoise: vec containing sound (not necessarily whitenoise)
  dur: total duration of the stimulus (secs)
  nframes: total frames in the whole stimulus
  samplingR: soundcard sampling rate (ie 96000). Need to match with EVERYTHING
  variance: fixed var
  randomized: shuffles noise vec
  paired: each instantaneous evidence is paired with its counterpart so their sum = 1
  returns: left noise vec, right noise vec, left coh stairs, right coh stairs [being them all 1d-arrays] 
  '''
  totpoints= dur*samplingR # should be an integer
  if len(whitenoise)<totpoints:
    return 'whitenoise is shorter than expected', 0, 0, 0
  if randomized==True:
    svec = whitenoise[:int(totpoints)]
    svec = svec.reshape(int(len(svec)/10),10)
    np.random.shuffle(svec)
    svec = svec.flatten()
  else:
    svec = whitenoise[:int(totpoints)]


  modfreq = nframes/dur
  modwave = 1*np.sin(2*np.pi*(modfreq)*np.arange(0, dur, step=1/samplingR)+np.pi)

  
  if coh<0 or coh>1:
    raise ValueError('invalid coh (<0 | >1)')
    return "invalid coherence, need (0~1)", 0, 0, 0
  elif coh==0 or coh==1:
    staircaseR = np.repeat(coh, dur*samplingR)
    staircaseL = staircaseR-1
    Lout = staircaseL*svec*modwave*LAmp
    Rout = staircaseR*svec*modwave*RAmp
    return Lout, Rout, np.repeat(coh-1,nframes), np.repeat(coh, nframes)
  elif coh<=(variance*1.1) or coh>=1-variance*1.1:
    raise ValueError('invalid coherence for given variance or viceversa (if coh!=0|1, 1.1*var<coh<1-var*1.1)')
  else:
    alpha = ((1-coh)/variance -1/coh)*coh**2
    beta = alpha*(1/coh-1)
    stairs_envelopeR = np.random.beta(alpha,beta, size=nframes)
    staircaseR = np.repeat(stairs_envelopeR, int(totpoints/nframes))
    staircaseL = staircaseR-1
    Rout=staircaseR*svec*modwave*RAmp
    if paired==False:
      stairs_envelopeL = np.random.beta(alpha,beta,size=nframes)-1
      staircaseL=np.repeat(stairs_envelopeL, int(totpoints/nframes))
      Lout=staircaseL*svec*modwave*LAmp
      return Lout, Rout, stairs_envelopeL, stairs_envelopeR
    Lout = staircaseL*svec*modwave*LAmp
    return Lout, Rout, stairs_envelopeR-1, stairs_envelopeR

def block(ss, repetitive=True, bsize=200, bnum=10):
    '''
    ss: int, starting side; 0=left; 1=right
    repetitive: bool, repetitve or alt block (prob 80/20 but swapped)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from repetitive to alternating)
    returns 1-d array of 0 and 1
    '''
    # some security checks
    if ss not in [0,1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(repetitive, bool) != True:
        raise ValueError('repetitive: bool, True->repetitive block; False-> alternating block')
    if isinstance(bsize, int)!=True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum>=1 == True):
        raise ValueError('bnum: int <= 1')
    out = [ss]
    prob_repeat = []
    for j in range(bnum):
        if repetitive == True:
            prob = [0.8, 0.2]
            prob_repeat = prob_repeat + [prob[0]]*bsize
        else:
            prob = [0.2, 0.8]
            prob_repeat = prob_repeat + [prob[0]]*bsize
        rep_alt_vec = np.random.choice([0,1], bsize, replace=True, p=prob) # being 0 repeat and 1 alt
        #generate L/R vec from starting point
        for i in range(len(rep_alt_vec)):
            out.append(abs(out[-1]-rep_alt_vec[i]))
        repetitive = not repetitive # once finished block, next one will be a different one
    return out, prob_repeat
# legacy and semantically incorrect stuff
def evidencetocoh(x):
    return (x+1)/2
def cohtoevidence(x):
    return 2*x-1

def select_evidence(trialtype, ev_list): # debug
    ''' for the sake of reducing 0-coh prob(1/2) and ease of use
    trialtype: int, 0=left, 1=right
    ev_list: array, contains all posible evidences.
    returns: COHERENCE from 0 (left) to 1(right). ie. 0 net evidence returns 0.5. Why?
    So we can calc beta distr directly. See envelope funct.
    '''
    if trialtype == 0:
        available = ev_list[ev_list<=0] # evidences ccorresponding to Left
    else:
        available = ev_list[ev_list>=0] # evidences corresponding to Right trials
    if len(np.unique(ev_list==0))==1: # All falses no 0, all fine
        selected_evidence = np.random.choice(available)
    else: # 0 evidence, where?
        zeroloc = np.where(available==0)[0][0] # index of item==0 in our vector available (available evidences for that particular reward side)
        currprob = 1/len(ev_list)
        prob_vec = np.repeat(currprob*2, len(available))
        prob_vec[zeroloc]=currprob # 1/2 prob of evidence=0 (so if you add both rew sides ev=0 ; ev=0 appears  with the same prob thhat other particular ev (eg -1 and 1))
        
        selected_evidence = np.random.choice(available, p=prob_vec)
    return (selected_evidence+1)/2

def antibias(bumpside, window, reward_side_vec, bump_prob, curr_trial_idx):
    '''
    bumpside: int ([0,1]), left or right, side to incr probability
    window: int >=1, window len of trials to apply antibias
    reward_side_vec: list, containing 0 and 1s (0, left; 1, right)
    bump_prob: float, from 0 to 1. prob that trials != bumpside within window shift.
    returns: reward_side_vec with updated window
    '''
    trialWin = np.array(reward_side_vec[curr_trial_idx+1:curr_trial_idx+window+1])
    loc=np.where(trialWin!=bumpside)[0] # getting location of trials to switch
    if bumpside==0:
        replacement = [1, 0] # ones likely to be replaced with 0s' with bump_prob
    elif bumpside==1:
        replacement = [0, 1] # 0s likely to be replaced with 1s' with bump prob.
    alt_vec = np.random.choice(replacement, len(loc), replace=True, p=[1-bump_prob, bump_prob]) # replacement vector 
    trialWin[loc] = alt_vec
    reward_side_vec[curr_trial_idx+1:curr_trial_idx+window+1]=trialWin.tolist()
    return reward_side_vec



def dropdownmenu(titlestr, labelstr, options_dic, default_opt):
    '''since it was easy to forget updating variations for logging purposes, this function helps generating a dropdown popup
    titlestr: String, window title
    labelstr: String, label 
    options_dic: akward dict (just comma delimited keys,) containing dropdown menu options
    default_opt: default option from the above dict keys'''
    root = Tk()
    root.title(titlestr)

    def show_entry_fields():
        global dummy 
        dummy = tkvar.get()
        root.destroy()

    # Add a grid
    mainframe = Frame(root)
    mainframe.grid(column=0,row=0, sticky=(N,W,E,S) )
    mainframe.columnconfigure(0, weight = 1)
    mainframe.rowconfigure(0, weight = 1)
    mainframe.pack(pady = 100, padx = 50)
    Button(mainframe, text='Quit', command=mainframe.quit).grid(row=3, column=0, sticky=W, pady=4)
    Button(mainframe, text='Confirm', command=show_entry_fields).grid(row=3, column=1, sticky=W, pady=4)

    # Create a Tkinter variable
    tkvar = StringVar(root)

    # Dictionary with options
    dropdownchoices = options_dic
    tkvar.set(default_opt) # set the default option

    popupMenu = OptionMenu(mainframe, tkvar, *dropdownchoices)
    Label(mainframe, text=labelstr).grid(row = 1, column = 1)
    popupMenu.grid(row = 2, column =1)

    # on change dropdown value
    def change_dropdown(*args):
        print( tkvar.get() )

    # link function to change dropdown
    tkvar.trace('w', change_dropdown)

    root.mainloop()

    return dummy

def block_side_enhanced(ss, rightenhanced=True, bsize=200, bnum=10, randomblock=False):
    '''
    ss: int, starting side; 0=left; 1=right
    rightenhanced: bool, right enhanced (True, 80%) or else (False --> left enhanced, 80%)
    bsize: blocksize (int)
    bnum: number of blocks to output (alternating from right enhanced to left enhanced side)
    returns 1-d array of 0 and 1
    '''
    # some security checks
    if ss not in [0,1]:
        raise ValueError('ss: starting side, int, either 0 (left) or 1(right)')
    if isinstance(rightenhanced, bool) != True:
        raise ValueError('rightenhanced: bool, True-> right enhanced; False-> left enhanced')
    if isinstance(bsize, int)!=True:
        raise ValueError('bsize: int, blocksize')
    if not (isinstance(bnum, int) and bnum>=1 == True):
        raise ValueError('bnum: int <= 1')

    # Initialize output vectors
    right_left_vec = [ss]
    if ss==0:
        side_enhanced = ['L']
    else:
        side_enhanced = ['R']

    # Generates the blocks and label them accordingly (R or L)
    for j in range(bnum):
        if rightenhanced==True:
            if len(right_left_vec)==1:
                side_enhanced.extend('R'*(bsize-1))
            else:
                side_enhanced.extend('R'*(bsize))
        elif rightenhanced==False:
            if len(right_left_vec)==1:
                side_enhanced.extend('L'*(bsize-1))
            else:
                side_enhanced.extend('L'*(bsize))

        #generate L/R vector
        for trial in range(1,bsize):
            # If R enhanced and previous was R
            if rightenhanced==True and right_left_vec[-1]==1:
                current_trial = np.random.choice([0,1], 1, replace=True, p=[0.24, 0.76])
                right_left_vec.append(current_trial[0])
            # If R enhanced and previous was L
            elif rightenhanced==True and right_left_vec[-1]==0:
                current_trial = np.random.choice([0,1], 1, replace=True, p=[0.04, 0.96])
                right_left_vec.append(current_trial[0])
            # If L enhanced and previous was L
            elif rightenhanced==False and right_left_vec[-1]==0:
                current_trial = np.random.choice([0,1], 1, replace=True, p=[0.76, 0.24])
                right_left_vec.append(current_trial[0])
            # If L enhanced and previous was R
            elif rightenhanced==False and right_left_vec[-1]==1:
                current_trial = np.random.choice([0,1], 1, replace=True, p=[0.96, 0.04])
                right_left_vec.append(current_trial[0])

        # once finished block, next one will be a different one (do we really want that?)
        if randomblock:
            rightenhanced=bool(np.random.choice([0,1]))
        else:
            rightenhanced = not rightenhanced

    return right_left_vec, side_enhanced


def newBlock(tm, ss='None', sblock='None', bnum=26, blen=80, block_seq='None', block_lens='None'):
	'''if any block_seq and are provided, this function should ignore bnum and/or blen, huehue future to do
	tm: transition matrix 2d array; rows being block type, and cols p(repeat when L, repeat when R), complementary p(alt) will be 1-p(rep) since its 2afc
	ss: starting side (0=L, 1=R)
	sblock= starting block (row index of corresponding probs in tm)
	bnum = total blocks
	blen = trial length of each block
	block_seq= list of blocktypes
	block_lens= list of lengths (1 item = 1 block)
	'''  
	# since pybpod is just getting strs ok we'll just use str Nones
	if ss=='None':
		ss=np.random.choice([0,1])
	if sblock=='None': # this shouldnt happen inside the function because it's desirable to log first Block type
	 	sblock = np.random.choice(np.arange(tm.shape[0]))
	if block_seq == 'None': 
		block_seq = np.tile(np.roll(np.arange(tm.shape[0]),-sblock), int(bnum/tm.shape[0])+tm.shape[0])[:bnum]
	if block_lens == 'None':
		block_lens = np.repeat(blen, bnum)
	out = [ss]
	prob_repeat = [] # len = len(out)-1 !! # agree about how to stardartize this, leaving it as it was
	# first block having 1st arbitrary side
	for btype, curr_blen in zip(block_seq, block_lens): # ensure both zipped lists/arrays have the same length, else it can lead to unexpected behavior will happen
		#print(f'iteration: {i}, block type: {btype}, blen: {curr_blen}')
		for j in range(curr_blen):
			c_r_prob = tm[btype,out[-1]]
			out += [np.random.choice([out[-1],(out[-1]-1)**2], p=[c_r_prob,1-c_r_prob])]
			prob_repeat += [c_r_prob]
		
	return out, prob_repeat
